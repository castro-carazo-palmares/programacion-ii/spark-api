/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import data.DatabaseManager;
import entidades.Ciudad;
import entidades.Persona;
import java.util.List;

/**
 *
 * @author Esteban Ramírez Martinez
 */
public class Servicio {

    DatabaseManager controlador = new DatabaseManager();

    public List<Persona> getPersonas() {
        controlador.abrirBD();
        List<Persona> personas = controlador.getPersonas();
        controlador.cerrarBD();
        return personas;
    }
    
    public List<Ciudad> getCiudades() {
       controlador.abrirBD();
       List<Ciudad> ciudades = controlador.getCuidades();
       controlador.cerrarBD();
       return ciudades;
    }

    public Persona getPersona(int cedula) {
        controlador.abrirBD();
        Persona persona = controlador.getPersona(cedula);
        controlador.cerrarBD();
        return persona;
    }
    
    public Ciudad getCiudad(int id) {
        controlador.abrirBD();
        Ciudad ciudad = controlador.getCiudad(id);
        controlador.cerrarBD();
        return ciudad;
    }
    
    public String postPersona(Persona persona) {
        controlador.abrirBD();
        Persona consulta = controlador.getPersona(persona.getCedula());
        controlador.cerrarBD();

        if (consulta == null) {
            controlador.abrirBD();
            controlador.insertarPersona(persona);
            controlador.cerrarBD();
            return "Persona con la cédula " + persona.getCedula() + " ha sido creada!";
        } else {            
            return "Persona ya existe";
        }
    }
    
    public String postCiudad(Ciudad ciudad) {
        controlador.abrirBD();
        int id = controlador.insertarCiudad(ciudad);
        controlador.cerrarBD();
        return "Ciudad con el id " + id + " ha sido creado!";
    }

    public String putPersona(Persona persona) {
        controlador.abrirBD();
        Persona consulta = controlador.getPersona(persona.getCedula());
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.actualizarPersona(persona);
            controlador.cerrarBD();
            return "Persona con la cédula " + persona.getCedula() + " ha sido actualizada!";
        } else {
            
            return "Persona no encontrada";
        }
    }
    
    public String putCiudad(Ciudad ciudad) {
        controlador.abrirBD();
        Ciudad consulta = controlador.getCiudad(ciudad.getId());
        controlador.cerrarBD();

        if (consulta != null) {
            controlador.abrirBD();
            controlador.actualizarCiudad(ciudad);
            controlador.cerrarBD();
            return "Ciudad con el id " + ciudad.getId()+ " ha sido actualizado!";
        } else {
            
            return "Ciudad no encontrada";
        }
    }
    
    public String deletePersona(int cedula) {

        controlador.abrirBD();
        Persona persona = controlador.getPersona(cedula);
        controlador.cerrarBD();

        if (persona != null) {
            controlador.abrirBD();
            controlador.eliminarPersona(cedula);
            controlador.cerrarBD();
            return "Persona con la cédula " + cedula + " ha sido eliminada!";
        } else {
            return "Persona no encontrada";
        }
    }
    
    public String deleteCiudad(int id) {

        controlador.abrirBD();
        Ciudad ciudad = controlador.getCiudad(id);
        controlador.cerrarBD();

        if (ciudad != null) {
            controlador.abrirBD();
            controlador.eliminarCiudad(id);
            controlador.cerrarBD();
            return "Ciudad con el id " + id + " ha sido eliminado!";
        } else {
            return "Ciudad no encontrada";
        }
    }
}
