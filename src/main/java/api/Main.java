/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static api.JsonUtil.json;
import entidades.Ciudad;
import entidades.Persona;
import static spark.Spark.*;

/**
 *
 * @author Esteban Ramírez Martinez
 */
public class Main {

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static void main(String[] args) {

        port(8080);
        Servicio servicio = new Servicio();

        // POST - Inserta una persona en la BD
        post("/persona", (request, response) -> {
            String cedula = request.queryParams("cedula");
            String nombre = request.queryParams("nombre");
            String apellido = request.queryParams("apellido");
            String ciudad = request.queryParams("ciudad");

            try {
                if(cedula != null && nombre != null && apellido != null && ciudad != null) {
                    Persona persona = new Persona(Integer.parseInt(cedula), nombre, apellido, ciudad);
                    return servicio.postPersona(persona);
                } else {
                    response.status(400);
                    return "Petición incorrecta";
                }
            } catch (NumberFormatException nfe) {
                return "Cédula no válida";
            }
        });
        
        // POST - Inserta una ciudad en la BD
        post("/ciudad", (request, response) -> {
            String nombre = request.queryParams("nombre");

            Ciudad ciudad = new Ciudad(0, nombre);
            return servicio.postCiudad(ciudad);
        });

        //GET - Devuelve todos las personas de la BD
        get("/persona", (request, response) -> {
            return servicio.getPersonas();
        }, json());
        
        //GET - Devuelve todos las ciudades de la BD
        get("/ciudad", (request, response) -> {
            return servicio.getCiudades();
        }, json());

        //GET - Devuelve una persona por el número de cédula
        get("/persona/:cedula", (request, response) -> {
            String cedula = request.params(":cedula");

            try {
                return servicio.getPersona(Integer.parseInt(cedula));
            } catch (NumberFormatException nfe) {
                return "Cédula no válida";
            }
        }, json());
        
        //GET - Devuelve una ciudad por el número de id
        get("/ciudad/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.getCiudad(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                return "Id no válido";
            }
        }, json());

        // PUT - Actualiza una persona por el número de cédula
        put("/persona/:cedula", (request, response) -> {
            String cedula = request.params(":cedula");
            String nombre = request.queryParams("nombre");
            String apellido = request.queryParams("apellido");
            String ciudad = request.queryParams("ciudad");

            try {
                Persona persona = new Persona(Integer.parseInt(cedula), nombre, apellido, ciudad);
                return servicio.putPersona(persona);
            } catch (NumberFormatException nfe) {
                return "Cédula no válida";
            }
        });
        
        // PUT - Actualiza una ciudad por el número de id
        put("/ciudad/:id", (request, response) -> {
            String id = request.params(":id");
            String nombre = request.queryParams("nombre");

            try {
                Ciudad ciudad = new Ciudad(Integer.parseInt(id), nombre);
                return servicio.putCiudad(ciudad);
            } catch (NumberFormatException nfe) {
                return "Id no válido";
            }
        });

        //DELETE - Elimina una persona por el número de cédula
        delete("/persona/:cedula", (request, response) -> {
            String cedula = request.params(":cedula");

            try {
                return servicio.deletePersona(Integer.parseInt(cedula));
            } catch (NumberFormatException nfe) {
                return "Cédula no válida";
            }
        });

        //DELETE - Elimina una ciudad por el número de id
        delete("/ciudad/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.deleteCiudad(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                return "Id no válido";
            }
        });
        
        //Authetication
        before((request, response) -> {
            String method = request.requestMethod();
            if (method.equals("GET") || method.equals("POST") || method.equals("PUT") || method.equals("DELETE")) {
                String authentication = request.headers("Authentication");
                if (!"CastroCarazo".equals(authentication)) {
                    halt(401, "Usuario no autorizado");
                }
            }
        });

        //CORS
        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
        });

    }

}
