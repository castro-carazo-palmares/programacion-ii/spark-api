/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import entidades.*;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase se encarga de comunicarse con la Base de Datos. Permite modificar,
 * agregar, extraer o eliminar datos.
 *
 * @author Esteban Ramírez Martinez
 */
public class DatabaseManager {

    private final String host = "localhost:3306";//Contiene el host de la BD.
    private final String database = "ejemplo";//Contiene el nombre de la BD.
    private final String url = "jdbc:mysql://" + host + "/" + database + "?autoReconnect=true&useSSL=false";//Contiene la dirección de la BD.
    private final String USER = "root";//Contiene el usuario de la BD.
    private final String PASS = "rootroot";//Contiene el password de la BD.
    private final String DRIVER = "com.mysql.jdbc.Driver";//Contiene la dirección del Driver de MySQL.
    Connection connection = null;//Inicializa la variable conexión a la BD.
    Statement statement = null;//Inicializa la variable statement de la BD.
    ResultSet resultSet = null;//Inicializa la variable tipo ResultSet.

    /**
     * Abre la Base de Datos.
     */
    public void abrirBD() {

        try {

            Class.forName(DRIVER);
            connection = (Connection) DriverManager.getConnection(url, USER, PASS);//Crea la conexión a la BD.
            statement = (Statement) connection.createStatement();//Crea el Statement.

        }//Fin de try
        catch (Exception ex) {

            System.err.println(ex);

        }//Fin de catch

    }//Fin de clase abrirBD

    public int insertarCiudad(Ciudad ciudad) {
        ResultSet result;
        int id = -1;
        
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "INSERT INTO Ciudad VALUES (?,?)";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, ciudad.getId());
            preparedStatement.setString(2, ciudad.getNombre());

            preparedStatement.executeUpdate();

            result = preparedStatement.getGeneratedKeys();

            if (result.first()) {
                id = result.getInt(1);
            }

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return id;
    }

    public void insertarPersona(Persona persona) {
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "INSERT INTO Persona VALUES (?,?,?,?)";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setInt(1, persona.getCedula());
            preparedStatement.setString(2, persona.getNombre());
            preparedStatement.setString(3, persona.getApellido());
            preparedStatement.setString(4, persona.getCiudad());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch
    }

    public void actualizarCiudad(Ciudad ciudad) {
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "UPDATE Ciudad SET nombre = ? WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setString(1, ciudad.getNombre());
            preparedStatement.setInt(2, ciudad.getId());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch
    }

    public void actualizarPersona(Persona persona) {
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "UPDATE Persona SET nombre = ?, apellido = ?, ciudad = ? WHERE cedula = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setString(1, persona.getNombre());
            preparedStatement.setString(2, persona.getApellido());
            preparedStatement.setString(3, persona.getCiudad());
            preparedStatement.setInt(4, persona.getCedula());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch
    }

    public void eliminarCiudad(int id) {

        try {

            String sql = "DELETE FROM Ciudad WHERE id=" + id;
            statement.execute(sql);

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

    }

    public void eliminarPersona(int cedula) {

        try {

            String sql = "DELETE FROM Persona WHERE cedula=" + cedula;
            statement.execute(sql);

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

    }

    public List<Ciudad> getCuidades() {
        List ciudades = new ArrayList();

        try {

            String sql = "SELECT * FROM Ciudad";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Ciudad ciudad = new Ciudad();
                ciudad.setId(resultSet.getInt("id"));
                ciudad.setNombre(resultSet.getString("nombre"));
                ciudades.add(ciudad);

            }

        } //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return ciudades;
    }

    public List<Persona> getPersonas() {
        List personas = new ArrayList();

        try {

            String sql = "SELECT * FROM Persona";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Persona person = new Persona();
                person.setCedula(resultSet.getInt("cedula"));
                person.setNombre(resultSet.getString("nombre"));
                person.setApellido(resultSet.getString("apellido"));
                person.setCiudad(resultSet.getString("ciudad"));
                personas.add(person);

            }

        } //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return personas;
    }

    public Ciudad getCiudad(int id) {
        Ciudad ciudad = null;

        try {
            String sql = "SELECT * FROM Ciudad WHERE id=" + id;
            resultSet = statement.executeQuery(sql);

            if (resultSet.first()) {

                ciudad = new Ciudad();
                ciudad.setId(resultSet.getInt(1));
                ciudad.setNombre(resultSet.getString(2));
                System.out.println(ciudad);
            }

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return ciudad;
    }

    public Persona getPersona(int cedula) {
        Persona person = null;

        try {
            String sql = "SELECT * FROM Persona WHERE cedula=" + cedula;
            resultSet = statement.executeQuery(sql);

            if (resultSet.first()) {

                person = new Persona();
                person.setCedula(resultSet.getInt(1));
                person.setNombre(resultSet.getString(2));
                person.setApellido(resultSet.getString(3));
                person.setCiudad(resultSet.getString(4));
                System.out.println(person);
            }

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return person;
    }

    /**
     * Cierra la Base de Datos.
     */
    public void cerrarBD() {

        try {

            statement.close();//Cierra el Statement.
            connection.close();//Termina la conxión con la BD.

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al cerrar la Base de Datos");

        }//Fin de catch

    }//Fin del método cerrarBD
}//Fin de clase
